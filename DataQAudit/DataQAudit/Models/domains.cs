﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pocReport.Models
{
    public class domainstt
    {
        public int domainId { get; set; } //primary key auto-increment
        public string clientName { get; set; }
        public string domainName { get; set; }
        public string publicKey { get; set; }
    }
}