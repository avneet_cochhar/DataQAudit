﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using pocReport.Models;
using Excel;
using System.Data;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text;
using DataQAudit;
using System.Globalization;
using System.Threading;
using LinqToExcel;

namespace pocReport.Controllers
{
    public class asyncUploadExcel:Controller
    {
        
        
        string date,prefix;
        DateTime dt ;
        CultureInfo provider = CultureInfo.InvariantCulture;
        StringBuilder sheetName = new StringBuilder();
        swabzEntities entity = new swabzEntities();
        static string decryptCompanyId = null, totalBytesString=null;
        static int companyId;
        int detailDefectId;
        static int totalFilesBytes = 0;
       

        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        SqlCommand cmd;
        string query, queryUpdateBytes;
        public async  Task<StringBuilder>  UploadExcelDataAsync(IEnumerable<HttpPostedFileBase> files,string encryptCompanyId)
        {
                totalFilesBytes =0;
            try
            {
                decryptCompanyId = encryptDecrypt.Decrypt(encryptCompanyId);
            }
            catch (Exception e)
            {
                return new StringBuilder("Please Login In");
            }
            try
            {
                companyId = Int32.Parse(decryptCompanyId);
                string fileName;
                try {
                    int fileContent = files.First().ContentLength;
                }
                catch (Exception e)
                {
                    return new StringBuilder("Please_Upload_a_file");
                }
                foreach (var file in files)
                {
                    fileName = file.FileName;
                    prefix = file.FileName.Substring(0, 7);
                    totalFilesBytes+= file.ContentLength;
                    if (prefix.Equals("Summary"))
                    {
                        date = fileName.Remove(0, 26);
                        date = date.Remove(date.Length - 5);
                        date = date.Replace("-", "/");
                        dt = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                        var dataValid = entity.defectSummaries.Where(a => a.RunDate.Equals(dt) && a.CompanyId.Equals(companyId)).FirstOrDefault();
                        if (dataValid != null)
                        {
                            sheetName.Append(file.FileName.Substring(0, 14) +"_"+ date+ "_Already_Available");
                            sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace


                        }
                        else
                        {
                            
                            if (storeSummaryData(file))
                            {
                                sheetName.Append(file.FileName.Substring(0, 14) + "_" + date + "_Sucessfully_Uploaded");
                                sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace


                            }
                        }
                        totalFilesBytes -= file.ContentLength;
                    }
                  
                }
                totalBytesString = totalFilesBytes.ToString();
                synchonizUploadBar();

                foreach (var file in files)
                {
                    prefix = file.FileName.Substring(0, 7);
                    if (file != null && file.ContentLength > 0)
                    {
                        fileName = file.FileName;
                        if (!prefix.Equals("Summary"))
                        {
                            //  var dataValid = entity.defectDetails.Where(a => a.DefectId.Equals(a.defectSummary.DefectId) && a.Rev_Fwd.Equals(prefix)&&a.defectSummary.Available.Equals("yes")&&a.defectSummary.DetailFileName.Equals(file.FileName)).FirstOrDefault();
                            var dataValid = entity.defectSummaries.Where(a => a.Available.Equals("yes") && a.DetailFileName.Equals(file.FileName)).FirstOrDefault();
                          
                            if (dataValid != null)
                            {
                                sheetName.Append(fileName.Substring(0, 14) + "_" +  fileName.Substring(fileName.Length - 32).Replace(" ","_") + "_Already_Available");
                                sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace

                                totalFilesBytes -= file.ContentLength;
                                synchonizUploadBar();
                            }
                            else
                            {
                                detailDefectId = await storeDefectDetails(file);
                                if (detailDefectId != 0)
                                {
                                    try
                                    {
                                        conn.Open();
                                        query = "UPDATE defectSummary SET Available='yes' WHERE DefectId=" + detailDefectId;
                                        cmd = new SqlCommand(query, conn);
                                        cmd.ExecuteNonQuery();
                                        sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace
                                        
                                        sheetName.Append(fileName.Substring(0, 14) + "_" + fileName.Substring(fileName.Length - 32).Replace(" ", "_") + "_sucessfully_uploaded");
                                        sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace

                                        conn.Close();

                                    }
                                    catch (Exception e)
                                    {
                                        
                                        sheetName.Append(fileName.Substring(0, 14) + "_" + fileName.Substring(fileName.Length - 32).Replace(" ", "_") + "_upload_error ");
                                        sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace

                                        conn.Close();
                                    }
                                }
                                else
                                {
                                   
                                    sheetName.Append(fileName.Substring(0, 7) + "_" + fileName.Substring(fileName.Length - 30).Replace(" ", "_") + "_failed");
                                    sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace

                                    totalFilesBytes -= file.ContentLength;

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                return new StringBuilder ("Either_Files_Not_Selected_or_Incorrect_File_Formate"); }
           
            
            return sheetName;
        }


       
        private async Task<int> storeDefectDetails(HttpPostedFileBase file)
        {
            
            decimal MaxProcessSize = 1000000;
            int redByte=0;
            byte[] fileByte;
            string[] rowData=null; string[] lines=null;
            int loop = 0;
            int chunk1=0, lastChunkSize;
            int LocalDetailDefectId;

            string fileName = file.FileName;
                       
            defectDetail defectDetails = new defectDetail();
            string[] uniqueCol = null;

            try {
                LocalDetailDefectId = entity.defectSummaries.Where(a => a.DetailFileName.Equals(file.FileName)).First().DefectId;
            }
            catch (Exception e) {
                 sheetName.Append("Summary_Not_Uploaded_");
               
                return 0;
            }
            
            try
            {
                int row = 7;
                string fileContent = file.ContentType;
                fileByte = new byte[file.ContentLength];
               
                decimal chunks = Math.Round(fileByte.Length / MaxProcessSize,1);
                string[] chunksParts = chunks.ToString().Split('.');
                chunk1 = Int32.Parse(chunksParts[0]);
                lastChunkSize = Int32.Parse(chunksParts[1]);

               
                fileByte = new byte[1 * (int)MaxProcessSize];         
                string fileByteData=System.Text.Encoding.UTF8.GetString(fileByte);
                


                while ((redByte = file.InputStream.Read(fileByte, 0, (int)MaxProcessSize)) > 0)                
                {                 

                    fileByteData = System.Text.Encoding.UTF8.GetString(fileByte);        
                    lines = fileByteData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                    

                    if (loop.Equals(0))
                    {
                        rowData = lines[6].Split(new string[] { "\t" }, StringSplitOptions.None);
                        uniqueCol = lines[6].Split(new string[] { "\t" }, StringSplitOptions.None);
                        
                    }

                    if (redByte.Equals((int)MaxProcessSize))
                    {
                        rowData = lines[lines.Length - 1].Split(new string[] { "\t" }, StringSplitOptions.None);
                        byte[] LastRowBytes = Encoding.Default.GetBytes(lines[lines.Length - 1].ToString());
                        file.InputStream.Seek(0 - LastRowBytes.Length, SeekOrigin.Current);
                    }
                    
                    synchonizUploadBar();
                    totalFilesBytes -= redByte;
                    insertRow(row,lines,uniqueCol, LocalDetailDefectId,redByte,chunk1,loop);                  
                    row = 0;
                    loop++;
                    
                   

                    if (loop.Equals(chunk1))
                        if ((lastChunkSize = file.InputStream.Read(fileByte, 0, (int)MaxProcessSize))< redByte)
                        {
                            fileByte = new byte[lastChunkSize];
                              MaxProcessSize = fileByte.Length;
                            file.InputStream.Seek(0 - (int)MaxProcessSize, SeekOrigin.Current);

                        }
                        else
                            file.InputStream.Seek(0 - lastChunkSize, SeekOrigin.Current);


                }//while loop
               
                
            }
            catch (Exception e)
            {
               
                sheetName.Append(fileName.Substring(0, 14) + "_" + fileName.Substring(fileName.Length - 32).Replace(" ", "_") + "_upload_error " + e.Message);
                sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace

                conn.Close();
                return 0;
            }
            conn.Close();
            
            return LocalDetailDefectId;
        }

        private void synchonizUploadBar()
        {
            try
            {
                conn.Close();
                conn.Open();
               // if (Int32.Parse(totalBytesString).Equals(totalFilesBytes))
              //      queryUpdateBytes = "UPDATE userWorksapce SET defectId=0,msg='" + totalBytesString + "' WHERE id=17";
              //  else
                queryUpdateBytes = "UPDATE userWorksapce SET defectId='" + totalFilesBytes + "',msg='" + totalBytesString + "' WHERE id=17";
                cmd = new SqlCommand(queryUpdateBytes, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {

            }
        }

        private void insertRow(int row,string[] lines, string[] uniqueCol,int id,int redByte, int chunk1, int loop)
        {
           
            string[] colData;
            string[] rowData =null;
            conn.Open();
            defectDetail defectDetails= new defectDetail();
            try
            {  
            
                for (int col = 0, n = 0; row <= (lines.Length-1); row++, col = 0, n = 0, defectDetails = new defectDetail())
                {                 
                    
                    rowData = lines[row].Split(new string[] { "\t" }, StringSplitOptions.None);
                   // synchonizUploadBar();
                  //  byte LastRowBytes = Encoding.Default.GetBytes(rowData[row].ToString());
                 //   totalFilesBytes -= LastRowBytes;

                    for (; col < (uniqueCol.Length - 7); col++)
                    {
                        if (col > 0)
                            defectDetails.UniqueId = defectDetails.UniqueId + "," + rowData[col];
                        else
                            defectDetails.UniqueId = rowData[col];

                    }

                    colData = new string[rowData.Length];
                    foreach (var obj in rowData.Skip(col))
                    {
                        colData[n++] = obj.ToString();
                    }
                    colData[4] = colData[4] == null ? "" : colData[4];

                    defectDetails.Rev_Fwd = prefix;
                    defectDetails.TargetColName = colData[0];
                    defectDetails.TargetValue = colData[1];
                    defectDetails.SourceValue = colData[2];
                    defectDetails.RowNum = colData[3];
                    defectDetails.ColNum = colData[4];
                    defectDetails.DefectId = id;

                    //   query = "INSERT INTO dbo.defectDetails (DefectId, Rev_Fwd, UniqueId, TargetColName, TargetValue, SourceValue, RowNum, ColNum) VALUES ("+ defectDetails.DefectId + ", '" + defectDetails.Rev_Fwd + "', '" + defectDetails.UniqueId + "' , '" + defectDetails.TargetColName + "' , '" + defectDetails.TargetValue + "' , '" + defectDetails.SourceValue + "' , '" + defectDetails.RowNum + "', '" + defectDetails.ColNum + "')";
                    query = "INSERT INTO defectDetails (DefectId, Rev_Fwd, UniqueId, TargetColName, TargetValue, SourceValue, RowNum, ColNum) VALUES (@DefectId, @Rev_Fwd,@UniqueId,@TargetColName,@TargetValue,@SourceValue,@RowNum,@ColNum)";

                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.Add("@DefectId", SqlDbType.Int);
                    cmd.Parameters.Add("@Rev_Fwd", SqlDbType.VarChar, 10);
                    cmd.Parameters.Add("@UniqueId", SqlDbType.VarChar, 200);
                    cmd.Parameters.Add("@TargetColName", SqlDbType.VarChar, 100);
                    cmd.Parameters.Add("@TargetValue", SqlDbType.VarChar, 50);
                    cmd.Parameters.Add("@SourceValue", SqlDbType.VarChar, 500);
                    cmd.Parameters.Add("@RowNum", SqlDbType.VarChar, 15);
                    cmd.Parameters.Add("@ColNum", SqlDbType.VarChar, 15);


                    cmd.Parameters["@DefectId"].Value = id;
                    cmd.Parameters["@Rev_Fwd"].Value = prefix;
                    cmd.Parameters["@UniqueId"].Value = defectDetails.UniqueId;
                    cmd.Parameters["@TargetColName"].Value = colData[0];
                    cmd.Parameters["@TargetValue"].Value = colData[1];
                    cmd.Parameters["@SourceValue"].Value = colData[2];
                    cmd.Parameters["@RowNum"].Value = colData[3];
                    cmd.Parameters["@ColNum"].Value = colData[4];

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                if (redByte != 0 && Enumerable.Range(chunk1, chunk1 + 1).Contains(loop) && row != 0)
                {
                    addTablesAuditedAndStatistics(uniqueCol, id, rowData, lines, loop, row);
                    entity.SaveChanges();
                    conn.Close();
                  //  return row;

                }
            }

            conn.Close();
        }

        private void addTablesAuditedAndStatistics(string[] uniqueCol,int defectId,string[] rowData,string[] lines,int loop,int row)
        {

            if (row != 0||loop!=0)
            {
                rowData = Regex.Split(lines[row+5], @"\D+");//lines[lines.Length - 11
                tablesAuditedRunStat localTARS = new tablesAuditedRunStat();
                localTARS.AuditCoverage = rowData[1];
                localTARS.RowChecked = rowData[2];
                localTARS.RowPassed = rowData[3];
                localTARS.MissinRows = rowData[4];
                localTARS.NonMissinsRowsError = rowData[5];
                localTARS.UniqueDataCellAudited = rowData[6];
                localTARS.UniqueDataCellPassed = rowData[7];
                localTARS.UniqueMissingDataCellAudited = rowData[8];
                localTARS.UniqueNonMissingDataCellError = rowData[9];
                localTARS.ReferenceId = defectId;
                entity.tablesAuditedRunStats.Add(localTARS);
            }

            rowData = lines[row+12].Split(new string[] { ",", "\t" }, StringSplitOptions.None);
            tablesAuditedColumn[] localTAC = new tablesAuditedColumn[(uniqueCol.Length - 7) + rowData.Length];

            for (int col = 0; col < (uniqueCol.Length - 7); col++)
            {
                localTAC[col] = new tablesAuditedColumn();
                localTAC[col].ColumnsAudited = uniqueCol[col];
                localTAC[col].ReferenceId = defectId;
                localTAC[col].UniqueKey = "TRUE";
                entity.tablesAuditedColumns.Add(localTAC[col]);
            }

            rowData = lines[row + 12].Split(new string[] { ",", "\t" }, StringSplitOptions.None);
            int uniqueLength = uniqueCol.Length - 7;

            foreach (var obj in rowData)
            {
                localTAC[uniqueLength] = new tablesAuditedColumn();
                localTAC[uniqueLength].ColumnsAudited = obj.ToString();
                localTAC[uniqueLength].ReferenceId = defectId;
                localTAC[uniqueLength].UniqueKey = "FALSE";
                entity.tablesAuditedColumns.Add(localTAC[uniqueLength]);
                uniqueLength++;

            }

           
        }

        private bool storeSummaryData(HttpPostedFileBase file)
        {
            //DateTime dt = DateTime.Parse("09/12/2009");
         
            string fileContent = file.ContentType;
            byte[] fileByte = new byte[file.ContentLength];
            var data = file.InputStream.Read(fileByte, 0, Convert.ToInt32(file.ContentLength));
            using (var package = new ExcelPackage(file.InputStream))
            {
                var currentsheet = package.Workbook.Worksheets;
                var worksheet = currentsheet.First();
                var noOfCol = worksheet.Dimension.End.Column;
                var noOfRow = worksheet.Dimension.End.Row;
                var domainId = entity.domains.Where(a => a.id.Equals(companyId)).FirstOrDefault();
                for (int row = 2; row <= noOfRow; row++)
                {
                    defectSummary summary = new defectSummary(); 
                        summary.AuditRun = worksheet.Cells[row, 1].Value.ToString();
                        summary.SourceTableName = worksheet.Cells[row, 2].Value.ToString();
                        summary.TargetTableName = worksheet.Cells[row, 3].Value.ToString();
                        summary.AuditResult = worksheet.Cells[row, 4].Value.ToString();
                        summary.DataAudited = Int32.Parse(worksheet.Cells[row, 5].Value.ToString());
                        summary.RowsAudited = Int32.Parse(worksheet.Cells[row, 6].Value.ToString());
                        summary.DataDefects = Int32.Parse(worksheet.Cells[row, 7].Value.ToString());
                        summary.RowsDefects = Int32.Parse(worksheet.Cells[row, 8].Value.ToString());
                        summary.DetailFileName = worksheet.Cells[row, 9].Value.ToString();
                        summary.CompanyId = domainId.id;
                        summary.RunDate = dt;
                        summary.Available = "no";
                    try
                    {                        
                       
                        conn.Open();
                      // SqlCommand cmd = new SqlCommand("INSERT INTO defectSummary (CompanyId, RunDate, AuditRun , SourceTableName, TargetTableName, AuditResult, DataAudited, RowsAudited, DataDefects, RowsDefects) VALUES (6, 'date', 'audit-run', 'audit-run', 'audit-run', 'audit-run', 23, 3, 3, 3); ", conn);
                        //   string query = "INSERT INTO defectSummary (CompanyId, RunDate, AuditRun , SourceTableName, TargetTableName, AuditResult, DataAudited, RowsAudited, DataDefects, RowsDefects) VALUES ("+summary.CompanyId+ ", " + summary.RunDate + "," + summary.AuditRun + "," + summary.SourceTableName + "," + summary.TargetTableName + "," + summary.AuditResult + "," + summary.DataAudited + "," + summary.RowsAudited + "," + summary.DataDefects + "," + summary.RowsDefects + ")";
                         query = "INSERT INTO defectSummary (CompanyId, RunDate, AuditRun , SourceTableName, TargetTableName, AuditResult, DataAudited, RowsAudited, DataDefects, RowsDefects,DetailFileName,Available) VALUES (@companyId, @RunDate,@AuditRun,@SourceTableName,@TargetTableName,@AuditResult,@DataAudited,@RowsAudited,@DataDefects,@RowsDefects,@DetailFileName,@Available)";

                        cmd = new SqlCommand(query, conn);

                        cmd.Parameters.Add("@CompanyId", SqlDbType.Int);                      
                         cmd.Parameters.Add("@RunDate", SqlDbType.Date);
                         cmd.Parameters.Add("@AuditRun", SqlDbType.VarChar, 20);
                         cmd.Parameters.Add("@SourceTableName", SqlDbType.VarChar, 20);
                         cmd.Parameters.Add("@TargetTableName", SqlDbType.VarChar, 20);
                         cmd.Parameters.Add("@AuditResult", SqlDbType.VarChar, 30);
                         cmd.Parameters.Add("@DataAudited", SqlDbType.Int);
                         cmd.Parameters.Add("@RowsAudited", SqlDbType.Int);
                         cmd.Parameters.Add("@DataDefects", SqlDbType.Int);
                         cmd.Parameters.Add("@RowsDefects", SqlDbType.Int);
                         cmd.Parameters.Add("@DetailFileName", SqlDbType.VarChar, 200);
                        cmd.Parameters.Add("@Available", SqlDbType.VarChar, 10);

                        cmd.Parameters["@CompanyId"].Value = summary.CompanyId;
                        cmd.Parameters["@RunDate"].Value = summary.RunDate;
                        cmd.Parameters["@AuditRun"].Value = summary.AuditRun;
                        cmd.Parameters["@SourceTableName"].Value = summary.SourceTableName;
                        cmd.Parameters["@TargetTableName"].Value = summary.TargetTableName;
                        cmd.Parameters["@AuditResult"].Value = summary.AuditResult;
                        cmd.Parameters["@DataAudited"].Value = summary.DataAudited;
                        cmd.Parameters["@RowsAudited"].Value = summary.RowsAudited;
                        cmd.Parameters["@DataDefects"].Value = summary.DataDefects;
                        cmd.Parameters["@RowsDefects"].Value = summary.RowsDefects;
                        cmd.Parameters["@DetailFileName"].Value = summary.DetailFileName;
                        cmd.Parameters["@Available"].Value = summary.Available;
                        cmd.ExecuteNonQuery();
                           conn.Close();
                    }
                    catch (Exception e)
                    {
                         sheetName.Append(file.FileName.Substring(0, 14) + "_" + file.FileName.Substring(file.FileName.Length - 15) + "_Uploaded_Failed");
                        sheetName.Append("                                                                                                                 ");//30 tabs -7 backspace

                        return false;
                    } 
                }
            }            
            return true;
        }        
    }
}