﻿using DataQAudit.Models;
using DataQAudit.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DataQAudit.Controllers
{
    public class summaryDefectTrendController : ApiController
    {
        DefectModel getLatestDefectObj = new DefectModel();
        // GET api/<controller>
        public latestDefectSummary[] GetLatestDefectTrend(string date, int DefectId,string SourceTableName,string TargetTableName)
        {
            return  getLatestDefectObj.GetLatestDefectTrend(date, DefectId, SourceTableName, TargetTableName);
        }

       

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}