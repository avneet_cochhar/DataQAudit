//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataQAudit
{
    using System;
    using System.Collections.Generic;
    
    public partial class defectDetail
    {
        public int id { get; set; }
        public int DefectId { get; set; }
        public string Rev_Fwd { get; set; }
        public string UniqueId { get; set; }
        public string TargetColName { get; set; }
        public string TargetValue { get; set; }
        public string SourceValue { get; set; }
        public string RowNum { get; set; }
        public string ColNum { get; set; }
    
        public virtual defectSummary defectSummary { get; set; }
    }
}
